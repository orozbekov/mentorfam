from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView

from apps.account.views import (
    LogoutAPIView,
    RegisterView,
    activate_view,
    LoginAPIView,
)

urlpatterns = [
    path("register", RegisterView.as_view(), name="register"),
    path(
        "activate/<str:activation_code>", activate_view, name="activate-email"
    ),
    path("login", LoginAPIView.as_view(), name="login"),
    path("logout", LogoutAPIView.as_view(), name="logout"),
    path("token/refresh", TokenRefreshView.as_view(), name="token-refresh"),
]
