from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils.translation import gettext_lazy as _
from rest_framework_simplejwt.tokens import RefreshToken
from apps.account.managers import *


class CustomUser(AbstractBaseUser, PermissionsMixin):
    ROLE_CHOICES = (
        (TUTOR, "Репетитор"),
        (STUDENT, "Студент"),
        (ADMIN, "Админ"),
    )

    username = None
    first_name = models.CharField(_("first name"), max_length=100, blank=True)
    last_name = models.CharField(_("last name"), max_length=150, blank=True)
    email = models.EmailField(_("email address"), unique=True)
    user_type = models.CharField(
        "Тип пользователя", max_length=20, choices=ROLE_CHOICES
    )

    is_superuser = models.BooleanField(
        _("superuser status"),
        default=False,
        help_text="Указывает, следует ли рассматривать этого пользователя как суперпользователя.",
    )
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_(
            "Designates whether the user can log into this admin site."
        ),
    )
    is_active = models.BooleanField(
        _("active"),
        default=False,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )

    created = models.DateTimeField("Дата создания", auto_now_add=True)
    updated = models.DateTimeField("Дата обновлении", auto_now=True)

    activation_code = models.CharField(max_length=17, blank=True, null=True)

    objects = CustomUserManager()

    USERNAME_FIELD = "email"
    EMAIL_FIELD = "email"
    REQUIRED_FIELDS = [
        "first_name",
        "last_name",
    ]

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        """
        Возвращает first_name плюс last_name с пробелом между ними.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def create_activation_code(self):
        from django.utils.crypto import get_random_string

        code = get_random_string(
            length=17,
            allowed_chars="qwertyuiopasdfghjklzxcvbnmQWERTYUIOPSDFGHJKLZXCVBNM1234567890",
        )
        self.activation_code = code

    def tokens(self):
        refresh = RefreshToken.for_user(self)
        return {"refresh": str(refresh), "access": str(refresh.access_token)}


class UserProfileBase(models.Model):
    user = models.OneToOneField(to=CustomUser, on_delete=models.CASCADE)
    profile_picture = models.ImageField(
        upload_to="profile_pics/", blank=True, null=True
    )
    education = models.CharField("Образование", max_length=150)
    social_media = models.URLField(blank=True)

    class Meta:
        abstract = True


class TutorProfile(UserProfileBase):
    experience = models.CharField("Стаж работы", max_length=100)
    experience_2 = models.CharField("Опыт работы", max_length=255)
    services_offered = models.CharField(max_length=200)
    hourly_rate = models.DecimalField(max_digits=5, decimal_places=2)


class StudentProfile(UserProfileBase):
    age = models.PositiveIntegerField("Возраст")
