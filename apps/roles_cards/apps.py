from django.apps import AppConfig


class RolesCardsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "apps.roles_cards"
